	<footer class="footer" itemscope itemtype="http://schema.org/WPFooter" role="contentinfo">
 		<div class="wrap">
  			<nav role="navigation">
				<?php wp_nav_menu(array(
					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
					'container_class' => 'footer-links',         // class of container (should you choose to use it)
					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
					'menu_class' => 'nav footer-nav',            // adding custom nav class
					'theme_location' => 'footer-links',             // where it's located in the theme
					'before' => '',                                 // before the menu
					'after' => '',                                  // after the menu
					'link_before' => '',                            // before each link
					'link_after' => '',                             // after each link
					'depth' => 0,                                   // limit the depth of the nav
					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
				)); ?>
			</nav>
		</div>
	</footer>
    
	<div class="cookie-infos">
		<div class="wrap cookie-infos__wrap clear">
			<div class="cookie-infos__texte">
				En poursuivant votre navigation, vous acceptez le dépôt de cookies tiers destinés à vous proposer des contenus et publicités ciblés et nous à nous permettre de procéder à des analyses statistiques d’audience et de navigation. Vous pouvez en savoir plus et paramétrer les cookies en <a href="#" title="Qu'est-ce qu'un cookie informatique ?">cliquant ici</a>
			</div>
			<button class="cookie-close cookie-infos__button">J'ai compris</button>
		</div>
	</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>
		
<!--                        `:'+###++;,`                            
                      .#@@@@@@@@@@@@@@@@@@@:                       
                   .@@@@@@@@@@@@@@@@@@@@@@@@@@@.                   
                  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#                 
                    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@.              
              ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@`            
           :@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@`          
          ;@@@@@@@@@@@@@@@@@,  #@@@@@@@@@@@@@@@@@@@@@@@@           
       ; @@@@@@@@@@@@@@@@@@@@    @@@@@@@@@@@@@@@@@@@@@@@@;         
      #@@@@@@@@@@@@@@@@+:'#@@      @@@@@@@@@@@@@@@@@@@@@@@@@,      
     #@@@@@@@@@@@@@@@@@              `#@@@@@@@@@@@@@@@@@@@@@@@+    
    ;@@@@@@@@@@@@@@@@@@,                :@@@@@@@@@@@@@@@@@@@@@@@   
    @@@@@@@@@@@@@@@@@@@                   +@@@@@@@@@@@@@@@@@@@@@@  
   @@@@@@@@@@@@@@+,`                         .,,,,,..```.:'@@@@@@@ 
  `@@@@@@@@@@@@@@`               @               ``         @@@@@@,
  @@@@@@@@@@@@@@@@@            @@@'           @@@@@@@@@.   @@@@@@@@
  @@@@@@@@@@@@@@@@@@            '+            @@@@@@:   ;@@@@@@@@@@
 `@@@@@@@@@@@@@@@@+                           @@@+   '@@@@@@@@@@@@@
 :@@@@@@@@@@@@@@'                             @,  +@@@@@@@@@@@@@@@@
 :@@@@@@@@@@@@@                                ,@@@@@@@@@@@@@@@@@@'
 :@@@@@@@@@@@@@@,                             @@@@@@@@@@@@@@@@@@@@ 
 `@@@@@@@@@@@@@@@@@#`                         @@@@@@@@@@@@@@@@@@@@ 
  @@@@@@@@@@@@@@@@@@@+                        '@@@@@@@@@@@@@@@@@@; 
  @@@@@@@@@@@@@@@@@@,                          @@@@@@@@@@@@@@@@@@  
  `@@@@@@@@@@@@@@@@                            +@@@@@@@@@@@@@@@@,  
   @@@@@@@@@@@@@@@                              @@@@@@@@@@@@@@@@   
 ;@@@@@@@@@@@@@@@                               @@@@@@@@@@@@@@@    
 @@@@@@@@@@@@@@@@                               #@@@@@@@,  '@@;    
 `@@@@@@@@@@@@@@:                               ;@@@@@@@@          
  ,@@@@@@@@@@@@@                                ,@@@@@@@@:         
   :@@@@@@@@@@@@                                ,@@@@@@@@#         
    ,@@@@@@@@@@#                                ,@@@@@@@@#         
     .@@@@@@@@@'    http://commmeunarbre.fr     ;@@@@@@@@.         
       @@@@@@@@.                                #@@@@@@@@          
        #@@@@@@                                 @@@@@@@@           
         `@@@@@                                 @@@@@@@            
           ,@@@                                `@@@@@,             
            +@@                                @@@@.               
             @@                                @:                  
              @                                                    
-->
</body>

</html> <!-- end of site. what a ride! -->
