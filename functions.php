<?php
/*
Author: Eddie Machado
URL: http://themble.com/bones/
*/

// LOAD BONES CORE (if you remove this, the theme will break)
require_once( 'library/bones.php' );

/*********************
LAUNCH BONES
Let's get everything up and running.
*********************/

function bones_ahoy() {

  // let's get language support going, if you need it
  load_theme_textdomain( 'bonestheme', get_template_directory() . '/library/translation' );

  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );
  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
  // ie conditional wrapper

  // launching this stuff after theme setup
  bones_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'bones_register_sidebars' );

  // cleaning up random code around images
  add_filter( 'the_content', 'bones_filter_ptags_on_images' );
  // cleaning up excerpt
  add_filter( 'excerpt_more', 'bones_excerpt_more' );

} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );


/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 680;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );

// Thumbnail sizes disponibles en back-office
add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
    ) );
}

/************* THEME CUSTOMIZE *********************/

function bones_theme_customizer($wp_customize) {

}

add_action( 'customize_register', 'bones_theme_customizer' );

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'bonestheme' ),
		'description' => __( 'The first (primary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
} // don't remove this bracket!


/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
  <div id="comment-<?php comment_ID(); ?>" <?php comment_class('cf'); ?>>
    <article  class="cf">
      <header class="comment-author vcard">
        <?php
        /*
          this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
          echo get_avatar($comment,$size='32',$default='<path_to_url>' );
        */
        ?>
        <?php
          // create variable
          $bgauthemail = get_comment_author_email();
        ?>
        <?php printf(__( '<cite class="fn">%1$s</cite> %2$s', 'bonestheme' ), get_comment_author_link(), edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ) ?>
        <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>

      </header>
      <?php if ($comment->comment_approved == '0') : ?>
        <div class="alert alert-info">
          <p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>
        </div>
      <?php endif; ?>
      <section class="comment_content cf">
        <?php comment_text() ?>
      </section>
      <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </article>
  <?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!

/*
* Désactivation des liens automatiques sur les images ajoutées via la bibliothèque des médias
*/
function cua_deactivate_imglink() {
	$image_set = get_option( 'image_default_link_type' );
	
	if ($image_set !== 'none') {
		update_option('image_default_link_type', 'none');
	}
}
add_action('admin_init', 'cua_deactivate_imglink', 10);

/*
* Simplification de l'interface d'administration
*/

// Simplification des menus
add_action( 'admin_menu', 'adjust_the_wp_menu', 999 );

function adjust_the_wp_menu() {

	$page = remove_menu_page('edit-comments.php');
	$page = remove_menu_page('tools.php');

	if ( current_user_can('editor') ) {
		$page = remove_submenu_page( 'themes.php', 'themes.php' );
	    $page = remove_submenu_page( 'themes.php', 'widgets.php' );
	    $page = remove_submenu_page( 'themes.php', 'customize.php' );
	    global $submenu;
	    unset($submenu['themes.php'][6]); // remove customize link
	}

	// Get editor role object
	$role_object = get_role( 'editor' );

	// Add capability to this role object
	$role_object->add_cap( 'edit_theme_options' );

}

// Suppression du menu "Options de l'écran", sauf pour les administrateurs
function cua_disable_screen_options( $show_screen ) {
    if ( current_user_can( 'manage_options' ) ) {
        return $show_screen;
    }
    return false;
}
add_filter( 'screen_options_show_screen', 'cua_disable_screen_options' );

// Retrait du panneau de bienvenue
remove_action('welcome_panel', 'wp_welcome_panel');

// Retrait des widgets du tableau de bord
function cua_remove_dashboard_widget() {
 	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
}
add_action('wp_dashboard_setup', 'cua_remove_dashboard_widget' );

// Colonnes des pages de liste
function cua_nice_bo_columns( $columns ) {
    unset($columns['author']);
	unset($columns['tags']);
    unset($columns['comments']);
    return $columns;
}
add_filter( 'manage_edit-post_columns', 'cua_nice_bo_columns', 10, 1 );
add_filter( 'manage_edit-page_columns', 'cua_nice_bo_columns', 10, 1 );

// Metaboxes - Général
add_filter('types_information_table', '__return_false'); // "Front-end display de WP Types"

// Metaboxes - Édition d'une page
function remove_page_fields() {
	remove_meta_box( 'commentstatusdiv' , 'page' , 'normal' );
	remove_meta_box( 'commentsdiv' , 'page' , 'normal' );
	remove_meta_box( 'authordiv' , 'page' , 'normal' );
	remove_meta_box( 'postcustom' , 'page' , 'normal' );
}
add_action( 'admin_menu' , 'remove_page_fields' );

// Metaboxes - Édition d'un post
function remove_post_custom_fields() {
	remove_meta_box( 'formatdiv' , 'post' , 'normal' );
	remove_meta_box( 'tagsdiv-post_tag' , 'post' , 'normal' );
	remove_meta_box( 'commentsdiv' , 'post' , 'normal' );
	remove_meta_box( 'revisionsdiv' , 'post' , 'normal' );
}
add_action( 'admin_menu' , 'remove_post_custom_fields' );

/*
 * Suppression du h1 et du h6 dans le menu déroulant des styles de l'éditeur WYSIWYG.
 */
add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );

function tiny_mce_remove_unused_formats($init) {
	// Add block format elements you want to show in dropdown
	$init['block_formats'] = 'Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Paragraph=p;Address=address;Pre=pre';
	return $init;
}

add_action('wp_dashboard_setup', function() {
  global $wp_meta_boxes;
  wp_add_dashboard_widget('custom_help_widget', 'Édition de la page d\'accueil', function() {
    echo '<p>Pour modifier les contenus de la page d\'accueil, vous pouvez cliquer sur le bouton ci-dessous.</p><p><a class="button button--default" href="' . home_url() . '/wp-admin/post.php?post=' . get_option('page_on_front') . '&action=edit">Éditer la page d\'accueil</a></p>';
  });
});

// ACF Google Maps API key fix
/* function fix_gmaps_api_key() {
	if(mb_strlen(acf_get_setting("google_api_key")) <= 0){
		acf_update_setting("google_api_key", "YOURAPIKEY");
	}
}
add_action( 'admin_enqueue_scripts', 'fix_gmaps_api_key' ); */

/**
 * Permet d'ajouter des colonnes dans les listes du BO
 */
/*
add_filter('manage_posts_columns', function($column) {
  $column['duree'] = 'Durée';
  $column['tempo'] = 'Tempo';
  $column['numero_sacem'] = 'Numéro SACEM';
  return $column;
});


add_action('manage_posts_custom_column', function($column_name) {
  if($column_name == 'duree') {
    echo get_field('duree');
  }
  if($column_name == 'tempo') {
    echo get_field('tempo');
  }
  if($column_name == 'numero_sacem') {
    echo get_field('numero_sacem');
  }
}, 10, 2);
*/

/**
 * Permet de débug facilement
 */
function debug($param) {
  echo '<pre>';
  print_r($param);
  echo '</pre>';
}

/**
 * Suppression des emojis
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/* DON'T DELETE THIS CLOSING TAG */ ?>
