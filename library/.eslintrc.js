module.exports = {
    "extends": "airbnb",
    "rules": {
        "indent": ["error", 4],
    },
    "installedESLint": true,
    "plugins": [
        "react"
    ]
};
