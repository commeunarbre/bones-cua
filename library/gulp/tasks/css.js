// MISC
const gulp       = require('gulp');
const config     = require('../../.gulprc.json');
const sourcemaps = require('gulp-sourcemaps');
const livereload = require('gulp-livereload');

// CSS
const postcss = require('gulp-postcss');
const cssimport = require('postcss-import');
const url = require('postcss-url');
const cssnext = require('postcss-cssnext');
const assets = require('postcss-assets');
const flexibility = require('postcss-flexibility');
//const stylelint = require('stylelint');
//const reporter = require('postcss-reporter');

module.exports = () => {
    const processors = [
        cssimport(),
        url(),
        flexibility(),
        cssnext({
            browsers: ['> 5%', 'last 2 versions', 'ie > 10', 'iOS 7']
        }),
        assets({ loadPaths: [config.paths.bmpDest, config.paths.svgDest] }),
        //stylelint(),
        //reporter({ clearMessages: true })
    ];
    return gulp.src(config.paths.cssSrc)
        .pipe(sourcemaps.init())
        .pipe(postcss(processors))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.paths.cssDest))
        .pipe(livereload());
};