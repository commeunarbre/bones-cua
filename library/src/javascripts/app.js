
global.jQuery = require("jquery")
const $ = jQuery;
//const slick = require('slick-carousel');
const svgxuse = require('svgxuse');
const flexibility = require('flexibility');
const Cookies = require('js-cookie');
//const modernizr = require('./bundles/modernizr-custom.js');
//const modaal = require('modaal/dist/js/modaal');

$(document).ready(() => {

	//Fallback pour l'object-fit
	//Utiliser la classe "of" sur le container de l'image � object-fit
	/*if ( ! Modernizr.objectfit ) {
        $('.of').each(function () {
            var $container = $(this),
                imgUrl = $container.find('img').prop('src');
            if (imgUrl) {
              $container
                .css('backgroundImage', 'url(' + imgUrl + ')')
                .addClass('of-fallback');
            }
        });
	}*/

	// Bandeau cookie
	const cookieName = 'cookieInfos_WEBSITE-NAME';
	const cookieInfos = Cookies.get('cookieInfos_PROJECT-NAME');

	if ( cookieInfos !== "true" ) {
		$('.cookie-infos').addClass('visible');
	}

	$('.cookie-close').click(function() {
		Cookies.set(cookieName, 'true', { expires: 180 });
		$('.cookie-infos').fadeOut('400', function() {
			$('.cookie-infos').removeClass('visible');
		});
	});

});
