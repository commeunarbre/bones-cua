const config     = require('./.gulprc.json');
const isDev      = config.env === 'dev';
const isProd      = config.env === 'prod';
const livereload = require('gulp-livereload');

const gulp = require('./gulp')([
    'css',
    'css_prod',
    'jsFrontBundle',
    'jsFrontLint',
    'imagesBitmap',
    'imagesSVG',
]);

const port = Math.floor(Math.random() * 10000);
console.log('Port utilisé : ' + port);

gulp.task('default', () => {
    isDev && livereload.listen(port);
    isDev && gulp.watch(config.paths.cssAll, ['css']);
    isDev && gulp.watch(config.paths.cssSubFold, ['css']);
    isProd && gulp.watch(config.paths.cssAll, ['css_prod']);
    isProd && gulp.watch(config.paths.cssSubFold, ['css_prod']);
    gulp.watch(config.paths.jsSrc, ['jsFrontLint', 'jsFrontBundle']);
    gulp.watch(config.paths.bmpSrc, ['imagesBitmap']);
    gulp.watch(config.paths.svgSrc, ['imagesSVG']);
});
